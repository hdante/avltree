#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct AVLTree AVLTree;
typedef struct TreeNode TreeNode;

struct TreeNode {
	TreeNode *left, *right;
	int value;
};

struct AVLTree {
	TreeNode *root;
};

#define max(x, y) ((x) > (y) ? (x) : (y))

#define CMD_INSERT 'i'
#define CMD_REMOVE 'r'
#define CMD_HEIGHT 'h'
#define CMD_PREORDER '<'
#define CMD_POSTORDER '>'
#define CMD_INORDER '='
#define CMD_TREE 't'
#define CMD_SEARCH '?'
#define CMD_QUIT 'q'

AVLTree *tree_new(void)
{
	AVLTree *tree;

	tree = malloc(sizeof(AVLTree));
	if (tree == NULL)
		return NULL;

	tree->root = NULL;

	return tree;
}

void node_delete(TreeNode *node)
{
	if (node != NULL) {
		node_delete(node->left);
		node_delete(node->right);
		free(node);
	}
}

void tree_delete(AVLTree *tree)
{
	node_delete(tree->root);
	free(tree);
}

bool node_search(TreeNode *node, int value)
{
	if (node == NULL)
		return false;

	if (node->value == value)
		return true;
	if (value < node->value)
		return node_search(node->left, value);

	return node_search(node->right, value);
}

TreeNode *node_new(int value)
{
	TreeNode *node;

	node = malloc(sizeof(TreeNode));
	if (node == NULL)
		return NULL;

	node->value = value;
	node->left = NULL;
	node->right = NULL;

	return node;
}

bool tree_search(AVLTree *tree, int value)
{
	return node_search(tree->root, value);
}

bool node_insert(TreeNode **node, int value)
{
	if (*node == NULL) {
		*node = node_new(value);
		if (*node == NULL)
			return false;

		return true;
	}

	if ((*node)->value == value)
		return true;

	if (value < (*node)->value)
		return node_insert(&(*node)->left, value);

	return node_insert(&(*node)->right, value);
}

bool tree_insert(AVLTree *tree, int value)
{
	return node_insert(&tree->root, value);
}

void node_print_tree(TreeNode *node, FILE *out)
{
	if (node == NULL)
		return;

	fprintf(out, "%d (", node->value);
	node_print_tree(node->left, out);
	fprintf(out, ") (");
	node_print_tree(node->right, out);
	fputc(')', out);
}

void tree_print_tree(AVLTree *tree, FILE *out)
{
	fputc('(', out);
	node_print_tree(tree->root, out);
	fputs(")\n", out);
}

void node_print_pre_order(TreeNode *node, FILE *out)
{
	if (node == NULL)
		return;

	fprintf(out, "%d ", node->value);
	node_print_pre_order(node->left, out);
	node_print_pre_order(node->right, out);
}

void tree_print_pre_order(AVLTree *tree, FILE *out)
{
	node_print_pre_order(tree->root, out);
	fputc('\n', out);
}

void node_print_post_order(TreeNode *node, FILE *out)
{
	if (node == NULL)
		return;

	node_print_post_order(node->left, out);
	node_print_post_order(node->right, out);
	fprintf(out, "%d ", node->value);
}

void tree_print_post_order(AVLTree *tree, FILE *out)
{
	node_print_post_order(tree->root, out);
	fputc('\n', out);
}

void node_print_in_order(TreeNode *node, FILE *out)
{
	if (node == NULL)
		return;

	node_print_in_order(node->left, out);
	fprintf(out, "%d ", node->value);
	node_print_in_order(node->right, out);
}

void tree_print_in_order(AVLTree *tree, FILE *out)
{
	node_print_in_order(tree->root, out);
	fputc('\n', out);
}

unsigned node_get_height(TreeNode *node)
{
	unsigned l, r;

	if (node == NULL)
		return 0;

	l = node_get_height(node->left);
	r = node_get_height(node->right);

	return 1 + max(l, r);
}

unsigned tree_get_height(AVLTree *tree)
{
	return node_get_height(tree->root);
}

bool node_remove(TreeNode **node, int value)
{
	TreeNode *tmp;

	if (*node == NULL)
		return false;

	if (value < (*node)->value)
		return node_remove(&(*node)->left, value);
	else if (value > (*node)->value)
		return node_remove(&(*node)->right, value);

	if ((*node)->left != NULL && (*node)->right != NULL) {
		tmp = *node;
		node = &(*node)->right;

		while ((*node)->left != NULL)
			node = &(*node)->left;

		tmp->value = (*node)->value;
	}

	if ((*node)->left == NULL)
		tmp = (*node)->right;
	else
		tmp = (*node)->left;

	free(*node);
	*node = tmp;

	return true;
}

bool tree_remove(AVLTree *tree, int value)
{
	return node_remove(&tree->root, value);
}

// menu() - terminal interface to update a binary tree
//
// Accepts textual commands from the standard input and updates the
// given binary tree, until then input file ends or the quit command is
// executed.  Each command is given in one input line. The command
// syntax is one character representing the command (i, r, ?, h, etc.),
// follwed by its arguments. Example:
// 	i 10 20    # inserts value 10, then value 20 in the tree
// 	h          # prints tree height
// 	<          # prints tree in pre-order sequence
// 	q          # quit program
//
// Maximum allowed input line is 255 bytes.
//
// Parameters:
// - tree: the binary tree to be operated upon.
void menu(AVLTree *tree)
{
	// A buffer to hold the input line.
	char buff[255];
	// A pointer inside buff to parse values.
	char *p;
	// The command character read.
	char cmd;
	// An offset inside buff to help with parsing.
	int pos;
	// the command's arguments.
	int value;

	// Loop line by line, reading each line into the buffer, until
	// the end of file.
	while(fgets(buff, sizeof(buff), stdin)) {
		// Prepare the pointer at the beginning of the buffer.
		p = buff;

		// Try to read the command character. Also stores the
		// last read position from the buffer.
		if (sscanf(p, " %c%n", &cmd, &pos) != 1) {
			// Malformed input, print error message and
			// continue the loop.
			fputs("Error: missing command\n", stderr);
			continue;
		}

		// Update the pointer position, skipping the command
		// just read.
		p += pos;

		// Check which command was input.
		switch(cmd) {
		// Insert command.
		case CMD_INSERT:
			// Loop over all values given for insertion,
			// reading each value per iteration, until there
			// are no more values.
			while (sscanf(p, "%d%n", &value, &pos) == 1) {
				// Try to insert the value just read in
				// the tree.
				if (!tree_insert(tree, value)) {
					// Out-of-memory, print error
					// message and abort program.
					perror("tree_insert()");
					abort();
				}
				// Update the pointer position, skipping
				// the value just read from the buffer.
				p += pos;
			}
			// Exit the command switch.
			break;
		// Remove command.
		case CMD_REMOVE:
			// Loop over all values given for removal,
			// reading each value per iteration, until there
			// are no more values.
			while (sscanf(p, "%d%n", &value, &pos) == 1) {
				// Try to remove the value just read
				// from the tree.
				if (!tree_remove(tree, value))
					// Value not found, just print
					// warning message and keep
					// going.
					fprintf(stderr, "Not found: %d\n", value);

				// Update the pointer position, skipping
				// the value just read from the buffer.
				p += pos;
			}
			// Exit the command switch.
			break;
		// Tree height command.
		case CMD_HEIGHT:
			// Output the full tree height, starting from
			// the root.
			printf("%u\n", tree_get_height(tree));
			// Exit the command switch.
			break;
		// Print tree pre-order command.
		case CMD_PREORDER:
			// Print all elements stored in the tree, in
			// pre-order.
			tree_print_pre_order(tree, stdout);
			// Exit the command switch.
			break;
		// Print tree post-order command.
		case CMD_POSTORDER:
			// Print all elements stored in the tree, in
			// post-order.
			tree_print_post_order(tree, stdout);
			// Exit the command switch.
			break;
		// Print tree in-order command.
		case CMD_INORDER:
			// Print all elements stored in the tree, in
			// order.
			tree_print_in_order(tree, stdout);
			// Exit the command switch.
			break;
		// Print tree nodes command.
		case CMD_TREE:
			// Print all elements stored in the tree, in
			// pre-order, in tree format (using parentheses
			// to identify the nodes).
			tree_print_tree(tree, stdout);
			// Exit the command switch.
			break;
		// Search command.
		case CMD_SEARCH:
			// Loop over all values given for insertion,
			// reading each value per iteration, until there
			// are no more values.
			while (sscanf(p, "%d%n", &value, &pos) == 1) {
				// Check if the value is in the tree.
				if (tree_search(tree, value))
					// If yes, print yes.
					fputs("yes ", stdout);
				else
					// If not, print no.
					fputs("no ", stdout);

				// Update the pointer position, skipping
				// the value just read from the buffer.
				p += pos;
			}
			// Print a newline on the output.
			fputc('\n', stdout);
			// Exit the command switch.
			break;
		// Quit command.
		case CMD_QUIT:
			return;
		// Unknown command fallback.
		default:
			// Warn about invalid command and then just keep
			// running the menu.
			fputs("Invalid command.\n", stderr);
		}
	}
}

int main(void)
{
	AVLTree *tree;

	tree = tree_new();
	if (tree == NULL) {
		perror("tree_new()");
		abort();
	}

	menu(tree);

	tree_delete(tree);

	return 0;
}
